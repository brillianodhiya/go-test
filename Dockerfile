FROM scratch

MAINTAINER Brilli brillidhiya@gmail.com

ADD main ./

ARG appname=go-test
ARG http_port=2408

ENTRYPOINT [ "/main" ]

EXPOSE $http_port
