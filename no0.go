package main

import (
	"fmt"
	"strings"
)

func getvocal(txt string) {
	vocal := []string{"a", "i", "u", "e", "o"}

	for _, a := range txt {
		if (a < 'a' || a > 'z') && (a < 'A' || a > 'Z') {
			fmt.Println("Terdeteksi Angka")
			return
		}
	}
	text := strings.Replace(strings.ToLower(txt), "", "", -1)
	reText := strings.Split(text, "")

	var reVocal []string
	var reConst []string

	for _, value := range reText {
		for i, j := range vocal {
			if value == j {
				if len(reVocal) != 0 {
					for l, m := range reVocal {
						if value == m {
							break
						} else if l == len(reVocal)-1 {
							reVocal = append(reVocal, value) //slice
						}
					}
				} else {
					reVocal = append(reVocal, value)
				}
				break
			} else if i == len(vocal)-1 {
				reConst = append(reConst, value)
			}
		}
	}

	fmt.Println("Huruf Mati : ", len(reConst))
	fmt.Println("Huruf Hidup : ", len(reVocal))
}

func main() {
	getvocal("omama")
}
