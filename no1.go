package main

import (
	"fmt"
	"strings"
)

func wordSorter(txt string) {
	vocal := []string{"a", "i", "u", "e", "o"}
	for _, a := range txt {
		if (a < 'a' || a > 'z') && (a < 'A' || a > 'Z') {
			fmt.Println("Terdeteksi Angka")
			return
		}
	}

	text := strings.Replace(strings.ToLower(txt), "", "", -1)
	reText := strings.Split(text, "")

	var hidup, mati []string
	var hasil string

	for _, value := range reText {
		for i, j := range vocal {
			if value == j {
				hidup = append(hidup, value) //slice
				break
			} else if i == len(vocal)-1 {
				mati = append(mati, value)
			}
		}
	}

	abc := []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}
	for _, a := range abc { // iterate data
		for _, b := range hidup {
			if a == b {
				hasil += b
			}
		}
	}

	for _, a := range abc {
		for _, b := range mati {
			if a == b {
				hasil += b
			}
		}
	}

	fmt.Println(hasil)
}

func main() {
	wordSorter("omama")
}
