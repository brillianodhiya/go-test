package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type Data struct {
	Hidup int `json:"Hidup"`
	Matu  int `json:"Mati"`
}

func countVocal(txt string) (x, y int) {
	vocal := []string{"a", "i", "u", "e", "o"}

	for _, a := range txt {
		if (a < 'a' || a > 'z') && (a < 'A' || a > 'Z') {
			fmt.Println("Terdeteksi Angka")
			return
		}
	}
	text := strings.Replace(strings.ToLower(txt), "", "", -1)
	reText := strings.Split(text, "")

	var reVocal []string
	var reConst []string

	for _, value := range reText {
		for i, j := range vocal {
			if value == j {
				if len(reVocal) != 0 {
					for l, m := range reVocal {
						if value == m {
							break
						} else if l == len(reVocal)-1 {
							reVocal = append(reVocal, value) //slice
						}
					}
				} else {
					reVocal = append(reVocal, value)
				}
				break
			} else if i == len(vocal)-1 {
				reConst = append(reConst, value)
			}
		}
	}

	x = len(reVocal)
	y = len(reConst)
	return
}

func dataHandle(txt http.ResponseWriter, request *http.Request) {
	txt.Header().Set("Content-Type", "application/json")

	if request.Method == "POST" {
		inputData := request.FormValue("data")

		if inputData == "" {
			http.Error(txt, "Data Kosong", http.StatusBadRequest)
			return
		}

		v, c := countVocal(inputData)
		data := Data{v, c}

		json.NewEncoder(txt).Encode(data)
		return
	}

	http.Error(txt, "", http.StatusBadRequest)
	return
}

func main() {
	PORT := "2408"

	http.HandleFunc("/", func(txt http.ResponseWriter, request *http.Request) {
		txt.Write([]byte("Here to get start, go to endpoint '/count' "))
	})
	http.HandleFunc("/count", dataHandle)

	fmt.Println("Server Running On Port", PORT)
	http.ListenAndServe(":"+PORT, nil)
}
