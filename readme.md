# Golang Test
> saya membuat project ini dengan meminta perpanjangan waktu selama 3 hari karena ini masih pertama kali nya menggunakan golang

### Singkat Penjelasan Tentang Golang
Go adalah bahasa pemrograman yang dibuat di Google pada tahun 2009 oleh Robert Griesemer, Rob Pike, dan Ken Thompson. Golang adalah bahasa pemrograman yang dihimpun dan diketik dalam bahasa C, dengan fitur pengumpulan sampah, penulisan terstruktur, keamanan memori, dan pemrograman yang konkuren serta berururtan.

## Challange

0. Buat fungsi sederhana untuk menghitung jumlah huruf hidup dan       huruf mati dari suatu kalimat. Contoh:   input: "omama"   hasil:    "huruf mati: 2, huruf hidup: 2"  (huruf hidup yaitu o dan a. a      tidak perlu muncul 2x). 
   #### Jawab: [no0.go](no0.go)
1. Buat fungsi sederhana untuk mengurutkan abjad dari suatu kalimat,   dengan memisahkan huruf hidup dan huruf mati. Contoh:
   input: "omama"   hasil: "aaomm" ('a', dan 'o' sebagai huruf hidup dan 'm' sebagai huruf mati)
   input: "osama"   hasil: "aaoms". 
   #### Jawab: [no1.go](no1.go)
2. Buat webservice untuk salah satu fungsi di atas (boleh pilih        nomor 0 atau nomor 1)
   #### Jawab: [no2.go](no2.go) 
   > saya menggunakan no 0 untuk fungsi webservice
3. Buat service docker untuk nomor 2
   #### Jawab:
   * ``` cd go-test ```
   * build ``` docker build -t go-test . ```
   * run ``` docker run -p 2408:2408 -t go-test ```